import base64
import rsa


class PublicKeyNotFound(Exception):
    def __str__(self):
        return "Public key not found!"


class PrivateKeyNotFound(Exception):
    def __str__(self):
        return "Private key not found!"


class Decipher:
    def __init__(self, public_key=None, private_key=None):
        self.__public_key = None
        self.__private_key = None

        if public_key:
            self.set_public_key(public_key)

        if private_key:
            self.set_private_key(private_key)

    def set_public_key(self, public_key):
        self.__public_key = rsa.PublicKey(*map(int, public_key.split(",")))

    def set_private_key(self, private_key):
        self.__private_key = rsa.PrivateKey(*map(int, private_key.split(",")))

    def encrypt(self, *args):
        if self.__public_key is None:
            raise PublicKeyNotFound

        if len(args) > 1:
            return [self.__encrypt(msg) for msg in args]

        return self.__encrypt(args[0])

    def __encrypt(self, message):
        return base64.b64encode(
            rsa.encrypt(message.encode("utf-8"), self.__public_key)
        ).decode()  # т.к. base64encode вернет byte-like

    def decrypt(self, *args):
        if self.__private_key is None:
            raise PrivateKeyNotFound

        if len(args) > 1:
            return [self.__decrypt(msg) for msg in args]

        return self.__decrypt(args[0])

    def __decrypt(self, msg):
        return rsa.decrypt(base64.b64decode(msg), self.__private_key).decode()
