YES = 'Yes'
NO = 'No'
ENTER_NAME = "Enter account name!"
ENTER_API_KEY = "Enter API KEY"
SUCCESS_API_KEY = "You entered the API KEY!"
ENTER_API_SECRET = "Enter the API SECRET"
SUCCESS_API_SECRET = "You entered the API SECRET!"
ENTER_LANGUAGE = "Choose language!"
BUTTON_ACCEPT_AGREEMENT = "I agree"
ACCEPT_AGREEMENT = "I agree"
BUTTON_PROPOSAL_OF_AGREEMENT = "Registration"
PROPOSAL_OF_AGREEMENT = (
    "By clicking the button you are aware of the entire risk of trading on a cryptocurrency exchange.\n "
    "And you have no complaints about the developers of the bot."
)
PROPOSAL_OF_REGISTRATION = (
    "This bot provides an automated trading tool on EXMO cryptocurrency exchange.\n\n"
    "To start you need to go through a short registration procedure!"
)
BUTTON_ACCEPT_REGISTRATION = "Sign up"
REGISTRATION_INFO = 'Name: {} \nAPI KEY: {} \nAPI SECRET: {}\n\n Confirm registration'
ACCOUNT_CHOICE = "Select account:\n"
KEY_INSTRUCTION = "API Key Generation Instructions!"
BUTTON_REGISTER_NEW_ACCOUNT = "Register new"
REGISTRATION_SUCCESS = "Registration successfully completed!"
REGISTRATION_FAILED = "Failed to register, try again later!"
MESSAGE_ERROR = "Oops An error has occurred! :C \n Developers are already notified!"
KEYS_EXISTS = "A user with these keys is already registered!"
BAD_SYMBOLS = "The message must consist of numbers and English letters!"
BAD_API_SECRET = 'The key must begin with "S-"'
BAD_API_KEY = 'The key must begin with "K-"'
BAD_KEYS = 'Failed to connect with the provided API keys!'
DELETE_CLIENT = 'Delete account'
CONFIRM_DELETING = 'Are you sure you want to delete your account: {}?'
DELETE_SUCCESS = 'Account successfully deleted!'
DELETE_FAIL = 'Error deleting account, try again later!'
