from aiogram import types


def choose_language():
    keyboard_markup = types.InlineKeyboardMarkup(row_width=2)
    keyboard_markup.add(
        types.InlineKeyboardButton("RUS 🇷🇺", callback_data="rus"),
        types.InlineKeyboardButton("ENG 🇦🇺", callback_data="eng"),
    )

    return keyboard_markup
