from aiogram import types
from .language.library import common
from .language.func import get_user_language
from .language.ui import choose_language
from .agreement.func import get_agreement
from .agreement.ui import accept_agreement
from .registration.func import get_one_client, clients_list
from .registration.ui import registration
from .menu.ui import show


def new_query(obj):
    if isinstance(obj, types.Message):
        return obj.answer
    if isinstance(obj, types.CallbackQuery):
        return obj.message.edit_text


async def check_user(dp, chat_id, query):
    query = new_query(query)
    if not await get_user_language(dp, chat_id):
        return await query(
            common.ENTER_LANGUAGE, reply_markup=choose_language()
        )

    if not await get_agreement(dp, chat_id):
        return await query(
            dp.lang.PROPOSAL_OF_AGREEMENT, reply_markup=accept_agreement()
        )

    if not await get_one_client(dp, chat_id):
        return await query(
            dp.lang.PROPOSAL_OF_REGISTRATION, reply_markup=registration()
        )

    return await query(
        dp.lang.ACCOUNT_CHOICE, reply_markup=show(await clients_list(dp, chat_id))
    )
