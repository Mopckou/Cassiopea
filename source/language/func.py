from db import Language


async def get_user_language(dp, chat_id):
    async with dp.pool.acquire() as conn:
        return await conn.fetchrow(
            Language.select().where(Language.c.chat_id == chat_id)
        )


async def input_language(dp, chat_id, language):
    async with dp.pool.acquire() as conn:
        await conn.execute(Language.insert().values(chat_id=chat_id, language=language))


async def update_language(dp, chat_id, language):
    async with dp.pool.acquire() as conn:
        await conn.execute(
            Language.update()
            .where(Language.c.chat_id == chat_id)
            .values(language=language)
        )
