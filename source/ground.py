from aiogram import types
from aiogram.dispatcher.filters import BoundFilter, ExceptionsFilter
from bot import dp
from .helpers import check_user


__all__ = [
    "command_start_command",
    "error_handler"
]


@dp.message_handler(commands=["start"])
async def command_start_command(message: types.Message):
    await check_user(dp, message.from_user.id, message)


@dp.errors_handler()
async def error_handler(update: types.Update, exception):
    if dp.sentry:
        print('OTPRAVKA')
        dp.sentry.captureException()

    if update.callback_query:
        return await update.callback_query.answer(dp.lang.MESSAGE_ERROR, show_alert=True)

    return await update.message.answer(
        dp.lang.MESSAGE_ERROR, reply=True, disable_notification=False
    )
