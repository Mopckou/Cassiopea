import asyncpgsa
from config import (
    POSTGRES_HOST,
    POSTGRES_PORT,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    POSTGRES_DATABASE,
)


async def create_connection_pool(dp):
    dp.pool = await asyncpgsa.create_pool(
        host=POSTGRES_HOST,
        port=POSTGRES_PORT,
        database=POSTGRES_DATABASE,
        user=POSTGRES_USER,
        # loop=event_loop,
        password=POSTGRES_PASSWORD,
        min_size=5,
        max_size=10,
    )


async def close_connection_pool(dp):
    await dp.pool.close()
