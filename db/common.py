from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)


class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, db.ForeignKey("chat.id"))
    name = db.Column(db.String)
    token = db.Column(db.String)
    password = db.Column(db.String)

    def __repr__(self):
        return f"User from chat_id : {self.chat_id}"


class Agreement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer(), db.ForeignKey("chat.id"))

    def __repr__(self):
        return f"Agreement from chat_id : {self.chat_id}"


class Language(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, db.ForeignKey("chat.id"))
    language = db.Column(db.String)

    def __repr__(self):
        return f"Language: {self.language} from chat: {self.chat_id}"
