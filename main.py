from aiogram.utils.executor import start_polling
from source import *
from db.postgres import create_connection_pool, close_connection_pool
from sentry import init_sentry, close_sentry

from bot.bot import *

on_startup = [
    create_connection_pool,
    init_sentry
]

on_shutdown = [
    close_connection_pool,
    close_sentry
]

if __name__ == "__main__":
    try:
        start_polling(dp, on_startup=on_startup, on_shutdown=on_shutdown, skip_updates=True)
    except Exception as e:
        print(e)
