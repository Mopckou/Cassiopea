from aiogram import Dispatcher
from aiogram import types


def show(clients):
    if not isinstance(clients, list):
        clients = [clients]

    keyboard_markup = types.InlineKeyboardMarkup(row_width=1)
    for client in clients:
        keyboard_markup.add(
            types.InlineKeyboardButton(client, callback_data=f"account_{client}"),
        )

    keyboard_markup.add(
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.BUTTON_REGISTER_NEW_ACCOUNT,
            callback_data="registration"
        ),
    )
    return keyboard_markup


def delete(client):
    keyboard_markup = types.InlineKeyboardMarkup(row_width=1)

    keyboard_markup.add(
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.DELETE_CLIENT,
            callback_data=f"menu of delete_{client}"
        ),
    )
    return keyboard_markup


def confirm_delete(client):
    keyboard_markup = types.InlineKeyboardMarkup(row_width=2)

    keyboard_markup.row(
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.YES, callback_data=f"delete_{client}"
        ),
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.NO, callback_data=f"account_{client}"
        ),
    )
    return keyboard_markup

