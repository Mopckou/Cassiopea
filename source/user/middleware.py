from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from common.repository import Repository, Cache
from common.aio_type import AioTypesNotFound
from .func import get_user, new_user
from source.common import Handlers


class UserMiddleware(BaseMiddleware, Handlers):
    def __init__(self,):
        self.cache = Cache()
        self.dispatcher = None
        self.handler = None
        self.chat_id = None
        super(UserMiddleware, self).__init__()

    async def on_process_callback_query(self, *args, **_):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        query = args[0]
        if not isinstance(query, types.CallbackQuery):
            raise AioTypesNotFound

        self.chat_id = query.from_user.id

        await self.process_user()

    async def on_process_message(self, *args, **_):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        message = args[0]
        if not isinstance(message, types.Message):
            raise AioTypesNotFound

        self.chat_id = message.from_user.id

        await self.process_user()

    async def process_user(self):
        if self.chat_id in self.cache and self.cache[self.chat_id].chat_id:
            self.cache[self.chat_id].update_time()
            self.cache.remove_expired()
            return True

        print(self.chat_id, type(self.chat_id))
        user = await get_user(self.dispatcher, self.chat_id)
        if not user:
            await new_user(self.dispatcher, self.chat_id)

        if self.chat_id in self.cache:
            self.cache[self.chat_id].chat_id = self.chat_id
            self.cache[self.chat_id].update_time()
            self.cache.remove_expired()
            return True

        self.cache[self.chat_id] = Repository(chat_id=self.chat_id)
        self.cache.remove_expired()
        return True


user_middleware = UserMiddleware()
