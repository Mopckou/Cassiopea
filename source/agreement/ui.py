from aiogram import types
from aiogram import Dispatcher


# def proposal_agreement():
#     keyboard_markup = types.InlineKeyboardMarkup(row_width=1)
#     keyboard_markup.row(
#         types.InlineKeyboardButton(Dispatcher.get_current().lang.BUTTON_PROPOSAL_OF_AGREEMENT,
#                                    callback_data='proposal of agreement'),
#     )
#
#     return keyboard_markup


def accept_agreement():
    keyboard_markup = types.InlineKeyboardMarkup(row_width=1)
    keyboard_markup.row(
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.BUTTON_ACCEPT_AGREEMENT,
            callback_data="accept agreement",
        ),
    )

    return keyboard_markup
