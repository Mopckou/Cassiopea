from db import Language
from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from source.language.library import eng, rus, common
from source.language.ui import choose_language
from common.repository import Repository, Cache
from common.aio_type import AioTypesNotFound
from source.common import Handlers
from .func import get_user_language


class LanguageMiddleware(BaseMiddleware, Handlers):

    journal = {"rus": rus, "eng": eng}

    def __init__(self,):
        self.cache = Cache()
        self.dispatcher = None
        self.chat_id = None
        self.handler = None
        super(LanguageMiddleware, self).__init__()

    async def on_process_callback_query(self, *args, **kwargs):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        query = args[0]
        if not isinstance(query, types.CallbackQuery):
            raise AioTypesNotFound

        self.chat_id = query.from_user.id
        print(
            self.chat_id,
            "cb",
            query.from_user.id,
            query.inline_message_id,
            query.message.chat.id,
        )
        if await self.process_language():
            return

        if self.handler in self.callback_handlers:
            return

        await query.message.edit_text(
            common.ENTER_LANGUAGE, reply_markup=choose_language()
        )
        raise CancelHandler()

    async def on_process_message(self, *args, **kwargs):
        self.dispatcher = Dispatcher.get_current()
        message = args[0]
        if not isinstance(message, types.Message):
            raise AioTypesNotFound

        self.chat_id = message.from_user.id
        if await self.process_language():
            return

        if self.handler in self.message_handlers:
            return

        await message.answer(common.ENTER_LANGUAGE, reply_markup=choose_language())
        raise CancelHandler()

    async def process_language(self):
        if self.chat_id in self.cache and self.cache[self.chat_id].language:
            user_cache = self.cache[self.chat_id]
            self.dispatcher.lang = self.journal[user_cache.language]
            user_cache.update_time()
            self.cache.remove_expired()
            return True

        result = await get_user_language(self.dispatcher, self.chat_id)
        if not result:
            return False

        language = result['language']
        if self.chat_id in self.cache:
            self.cache[self.chat_id].language = language
            self.cache[self.chat_id].update_time()
            self.dispatcher.lang = self.journal[language]
            self.cache.remove_expired()
            return True

        self.cache[self.chat_id] = Repository(language=language)
        self.dispatcher.lang = self.journal[language]
        self.cache.remove_expired()
        return True

    def clear_cache(self, chat_id):
        if chat_id in self.cache:
            self.cache.pop(chat_id)


language_middleware = LanguageMiddleware()
