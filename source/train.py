import json
import requests
import logging
import time
import aiohttp
import stem
from stem.control import Controller
from asyncio import sleep
from aiohttp_socks import SocksConnector
from config import TOKEN
import asyncio
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.redis import RedisStorage2
from aiogram.dispatcher import DEFAULT_RATE_LIMIT
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils.exceptions import Throttled

# Configure logging
logging.basicConfig(level=logging.INFO)
# storage = MemoryStorage()
# In this example Redis storage is used
# storage = RedisStorage2(db=5)
# Initialize bot and dispatcher
bot = Bot(token=TOKEN, proxy="socks5://127.0.0.1:9050")

dp = Dispatcher(bot)  # , storage=storage)


@dp.message_handler(regexp="(^cat[s]?$|puss)")
async def cats(message: types.Message):
    with open("data/cats.jpg", "rb") as photo:
        """
        # Old fashioned way:
        await bot.send_photo(
            message.chat.id,
            photo,
            caption='Cats are here 😺',
            reply_to_message_id=message.message_id,
        )
        """

        await message.reply_photo(photo, caption="Cats are here 😺")


@dp.message_handler(commands="start")
async def start_cmd_handler(message: types.Message):
    await message.reply("Hi!\nDo you love aiogram?")
    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    # default row_width is 3, so here we can omit it actually
    # kept for clearness

    text_and_data_exmo = (
        ("Сервис exmo: кнопка 1", "1"),
        ("Сервис exmo: кнопка 2", "2"),
        ("Сервис exmo: кнопка 3", "3"),
    )

    text_and_data_kraken = (
        ("Сервис kraken: кнопка 1", "4"),
        ("Сервис kraken: кнопка 2", "5"),
        ("Сервис kraken: кнопка 3", "6"),
    )

    # in real life for the callback_data the callback data factory should be used
    # here the raw string is used for the simplicity
    row_btns_1 = (
        types.InlineKeyboardButton(text, callback_data=data)
        for text, data in text_and_data_exmo
    )
    row_btns_2 = (
        types.InlineKeyboardButton(text, callback_data=data)
        for text, data in text_and_data_kraken
    )

    keyboard_markup.row(*row_btns_1)
    keyboard_markup.row(*row_btns_2)

    keyboard_markup.add(
        # url buttons have no callback data
        types.InlineKeyboardButton(
            "aiogram source", url="https://github.com/aiogram/aiogram"
        ),
    )
    await message.reply("Hi!\nDo you love aiogram?", reply_markup=keyboard_markup)


@dp.message_handler()
async def echo(message: types.Message):
    logging.info("taaaak")
    print(213)
    if message.text == "Э":
        return await message.reply("О")
    if message.text == "О":
        return await message.reply("Э")

    print(message.migrate_from_chat_id)
    print(message)
    #  await message.reply(message.reply_to_message.text)
    # old style:
    # await bot.send_message(message.chat.id, message.text)
    await message.answer(message.text, reply=True)


# # the on_throttled object can be either a regular function or coroutine
# async def hello_throttled(*args, **kwargs):
#     # args will be the same as in the original handler
#     # kwargs will be updated with parameters given to .throttled (rate, key, user_id, chat_id)
#     print(f"hello_throttled was called with args={args} and kwargs={kwargs}")
#     message = args[0]  # as message was the first argument in the original handler
#     await message.answer("Throttled")

#
# def rate_limit(limit: int, key=None):
#     """
#     Decorator for configuring rate limit and key in different functions.
#     :param limit:
#     :param key:
#     :return:
#     """
#
#     def decorator(func):
#         setattr(func, 'throttling_rate_limit', limit)
#         if key:
#             setattr(func, 'throttling_key', key)
#         return func
#
#     return decorator

#
# class ThrottlingMiddleware(BaseMiddleware):
#     """
#     Simple middleware
#     """
#
#     def __init__(self, limit=DEFAULT_RATE_LIMIT, key_prefix='antiflood_'):
#         self.rate_limit = limit
#         self.prefix = key_prefix
#         super(ThrottlingMiddleware, self).__init__()
#
#     async def on_process_message(self, message: types.Message, data: dict):
#         """
#         This handler is called when dispatcher receives a message
#         :param message:
#         """
#         # Get current handler
#         handler = current_handler.get()
#
#         # Get dispatcher from context
#         dispatcher = Dispatcher.get_current()
#         # If handler was configured, get rate limit and key from handler
#         if handler:
#             limit = getattr(handler, 'throttling_rate_limit', self.rate_limit)
#             key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
#         else:
#             limit = self.rate_limit
#             key = f"{self.prefix}_message"
#
#         # Use Dispatcher.throttle method.
#         try:
#             await dispatcher.throttle(key, rate=limit)
#         except Throttled as t:
#             # Execute action
#             await self.message_throttled(message, t)
#
#             # Cancel current handler
#             raise CancelHandler()
#
#     async def message_throttled(self, message: types.Message, throttled: Throttled):
#         """
#         Notify user only on first exceed and notify about unlocking only on last exceed
#         :param message:
#         :param throttled:
#         """
#         handler = current_handler.get()
#         dispatcher = Dispatcher.get_current()
#         if handler:
#             key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
#         else:
#             key = f"{self.prefix}_message"
#
#         # Calculate how many time is left till the block ends
#         delta = throttled.rate - throttled.delta
#
#         # Prevent flooding
#         if throttled.exceeded_count <= 2:
#             await message.reply('Too many requests! ')
#
#         # Sleep.
#         await asyncio.sleep(delta)
#
#         # Check lock status
#         thr = await dispatcher.check_key(key)
#
#         # If current message is not last with current key - do not send message
#         if thr.exceeded_count == throttled.exceeded_count:
#             await message.reply('Unlocked.')


# Use multiple registrators. Handler will execute when one of the filters is OK
@dp.callback_query_handler(text="1")  # if cb.data == 'no'
@dp.callback_query_handler(text="2")  # if cb.data == 'yes'
@dp.callback_query_handler(text="3")  # if cb.data == 'yes'
@dp.callback_query_handler(text="4")  # if cb.data == 'yes'
@dp.callback_query_handler(text="5")  # if cb.data == 'yes'
@dp.callback_query_handler(text="6")  # if cb.data == 'yes'
# @rate_limit(4, 'throlling')
async def inline_kb_answer_callback_handler(query: types.CallbackQuery):
    answer_data = query.data
    # always answer callback queries, even if you have nothing to say
    await query.answer(f"You answered with {answer_data!r}")
    await bot.send_chat_action(query.from_user.id, "typing")

    if answer_data == "1":
        before = time.ctime()
        async with test_semaphore_exmo:
            res = await exmo_get_cash(session_exmo)
        result = await get_exmo()
        after = time.ctime()
        return await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                1, before, after, res
            ),
        )
    elif answer_data == "2":
        before = time.ctime()
        result = await get_exmo()
        after = time.ctime()
        return await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                2, before, after, result
            ),
        )
    elif answer_data == "3":
        before = time.ctime()
        async with aiohttp.client.request(method="GET", url=exmo_url) as r:
            result = await r.text()
        after = time.ctime()
        return await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                3, before, after, result
            ),
        )
    elif answer_data == "4":
        before = time.ctime()
        result = await get_kraken()
        after = time.ctime()
        return await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                4, before, after, result
            ),
        )
    elif answer_data == "5":
        before = time.ctime()
        result = await get_exmo()
        after = time.ctime()
        await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                2, before, after, result
            ),
        )
        before = time.ctime()
        # looper = Looper()
        exmo2 = ExmoAPI(
            "K-1966865b5cae71f326010c6633cf13d84b8f0af8",
            "S-08602027a4dcfc02c3923f3173bda5210e73e51f",
        )
        exmo1 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo11 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo12 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo13 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo14 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo15 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo16 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo17 = ExmoAPI(
            "K-3e171fc1bafda87a09073771080ae5d497b0120b",
            "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
        )
        exmo3 = ExmoAPI(
            "K-029e33a2c5fd007fd1aca67da43550b363762048",
            "S-90b248a1917cd60c314c9fe344aafa4048f72768",
        )
        exmo_list = [exmo2] * 10 + [exmo3] * 11
        for num, value in enumerate(exmo_list):
            asyncio.ensure_future(Looper().loop(value, session_exmo, num))
        # await asyncio.gather(
        #     Looper().loop(exmo11, session_exmo, 11),
        #     Looper().loop(exmo12, session_exmo, 12, 'order_book'),
        #     Looper().loop(exmo13, session_exmo, 13),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo, 'order_book'),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo, 'order_book'),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo, 'order_book'),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo),
        # looper.loop(exmo1, session_exmo, 'order_book'),
        # looper.loop(exmo1, session_exmo)
        # )

        after = time.ctime()
        return await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                5, before, after, "Задания запущены!"
            ),
        )
    elif answer_data == "6":
        before = time.ctime()
        result = await get_kraken()
        after = time.ctime()
        return await bot.send_message(
            query.from_user.id,
            "button - {}\nbefore - {}\nafter - {}\nresult - {}".format(
                6, before, after, result
            ),
        )
    else:
        text = f"Unexpected callback data {answer_data!r}!"

    await bot.send_message(query.from_user.id, text)


class Looper:
    def __init__(self):
        self.count = 0
        self.err_count = 0
        self.too_fast_count = 0

    async def loop(self, client, session, num, method="user_info"):
        start = time.time()
        self.num = num
        print("Запрашиваем ЕКСМО, аккаунтом {}".format(client))
        while 1:
            if time.time() - start > 60:
                print(
                    "count:",
                    self.count,
                    ", error:",
                    self.err_count,
                    ", too_fast:",
                    self.too_fast_count,
                    " num:",
                    self,
                )
                break
            if self.count == 180:
                print("Запросов больше 180. Аккаунт {}".format(client))
                break
            params = {} if method == "user_info" else {"pair": "BTC_USD", "limit": 10}
            async with test_semaphore_exmo:
                # print('Запрос номер {}, аккаунт {}'.format(self.count, client))
                res = await client.api_query(session, method, params=params)
            await sleep(1)
            # print(res)
            if "error" in res and "40034" in res["error"]:
                self.err_count += 1
            elif "error" in res and "40009" in res["error"]:
                self.too_fast_count += 1
            else:
                self.count += 1

    def __str__(self):
        return "{}".format(self.num)


async def get_exmo():
    async with test_semaphore_exmo:
        return await fetch(session_exmo, exmo_url)


async def fetch(session, url):
    async with session.get(url) as s:
        return await s.text()


async def exmo_get_cash(session):
    exmo = ExmoAPI(
        "K-3e171fc1bafda87a09073771080ae5d497b0120b",
        "S-4d4e976cfad3726acbec8f2ea10a10143e403a14",
    )
    # return await exmo.api_query(session, 'user_info')
    # async with session.get(url) as s:
    #     return await s.text()


async def get_kraken():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate()
        controller.signal(stem.Signal.NEWNYM)
        print(controller.get_newnym_wait())
        await sleep(controller.get_newnym_wait())
    session_kraken = aiohttp.ClientSession(
        connector=SocksConnector.from_url("socks5://127.0.0.1:9050")
    )
    async with test_semaphore_kraken:
        f = await fetch(session_kraken, kraken_url)
        await session_kraken.close()
        return f


@dp.message_handler(commands=["start"])
# @rate_limit(5, 'start')  # this is not required but you can configure throttling manager for current handler using it
async def cmd_test(message: types.Message):
    # You can use this command every 5 seconds
    await message.reply("Test passed! You can use this command every 5 seconds.")


if __name__ == "__main__":
    # Setup middleware
    # dp.middleware.setup(ThrottlingMiddleware(4))

    # Start long-polling
    executor.start_polling(dp)
