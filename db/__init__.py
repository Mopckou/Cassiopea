from .common import Chat, Client, Language, Agreement

Chat = Chat.__table__
Client = Client.__table__
Language = Language.__table__
Agreement = Agreement.__table__
