from aiogram import types
from bot import dp
from aiogram.dispatcher import FSMContext
from .states import RegistrationState
from .func import (
    token_inquery,
    register_new_client,
    new_client, hide_key,
    good_symbols,
    check_api_key,
    check_secret_key
)
from ..helpers import check_user
from .ui import accept_registration


__all__ = [
    "process_registration",
    "input_name",
    "input_api_key",
    "input_api_secret",
    "confirm_registration",
]


@dp.callback_query_handler(state='*', text=["registration"])
async def process_registration(query: types.CallbackQuery):
    await RegistrationState.INPUT_NAME.set()
    await query.message.edit_text(dp.lang.ENTER_NAME, reply_markup=None)


@dp.message_handler(state=RegistrationState.INPUT_NAME)
async def input_name(message: types.Message, state: FSMContext):
    if not good_symbols(message.text):
        return await message.reply(dp.lang.BAD_SYMBOLS)

    async with state.proxy() as data:
        data["name"] = message.text

    await RegistrationState.next()
    await message.bot.send_chat_action(message.from_user.id, types.ChatActions.UPLOAD_PHOTO)
    await dp.bot.send_photo(
        message.from_user.id,
        open('./source/pic/how generate keys.png', 'rb'),
        dp.lang.KEY_INSTRUCTION
    )
    await message.bot.send_chat_action(message.from_user.id, types.ChatActions.UPLOAD_PHOTO)
    await dp.bot.send_photo(
        message.from_user.id,
        open('./source/pic/api key.png', 'rb'),
        dp.lang.ENTER_API_KEY
    )


@dp.message_handler(state=RegistrationState.INPUT_API_KEY)
async def input_api_secret(message: types.Message, state: FSMContext):
    if not good_symbols(message.text):
        return await message.reply(dp.lang.BAD_SYMBOLS)

    if not check_api_key(message.text):
        return await message.reply(dp.lang.BAD_API_KEY)

    async with state.proxy() as data:
        data["api_key"] = message.text

    await RegistrationState.next()
    await dp.bot.send_photo(
        message.from_user.id,
        open('./source/pic/secret key.png', 'rb'),
        dp.lang.ENTER_API_SECRET
    )


@dp.message_handler(state=RegistrationState.INPUT_API_SECRET)
async def input_api_key(message: types.Message, state: FSMContext):
    if not good_symbols(message.text):
        return await message.reply(dp.lang.BAD_SYMBOLS)

    if not check_secret_key(message.text):
        return await message.reply(dp.lang.BAD_API_SECRET)

    async with state.proxy() as data:
        data["api_secret"] = message.text

        name = data['name']
        api_key = hide_key(data["api_key"])
        api_secret = hide_key(data["api_secret"])

    info = dp.lang.REGISTRATION_INFO.format(name, api_key, api_secret)
    await RegistrationState.next()
    await message.answer(info, reply_markup=accept_registration())


@dp.callback_query_handler(state=RegistrationState.ACCEPT_REGISTRATION, text=["accept registration"])
async def confirm_registration(query: types.CallbackQuery, state: FSMContext):
    chat_id = query.from_user.id

    async with state.proxy() as data:
        name = data['name']
        api_key = data["api_key"]
        api_secret = data["api_secret"]

    status, result = await register_new_client(name, chat_id, api_key, api_secret)
    if status == 409:
        await state.finish()
        await query.answer(dp.lang.KEYS_EXISTS, show_alert=True)
        return await check_user(dp, chat_id, query)

    elif status == 400:
        await state.finish()
        await query.answer(dp.lang.BAD_KEYS, show_alert=True)
        return await check_user(dp, chat_id, query)

    elif status != 200:
        await state.finish()
        await query.answer(dp.lang.REGISTRATION_FAILED, show_alert=True)
        return await check_user(dp, chat_id, query)

    password = api_secret.replace("S-", "") + api_key.replace("K-", "")
    status, result = await token_inquery(name, password)

    if status != 200:
        await query.answer(dp.lang.REGISTRATION_FAILED, show_alert=True)
        await state.finish()
        return await check_user(dp, chat_id, query)

    await new_client(dp, chat_id, name, password, result["token"])
    await query.answer(dp.lang.REGISTRATION_SUCCESS, show_alert=True)

    await state.finish()
    await check_user(dp, chat_id, query)
