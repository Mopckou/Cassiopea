from db import Agreement


async def get_agreement(dp, chat_id):
    async with dp.pool.acquire() as conn:
        return await conn.fetchrow(
            Agreement.select().where(Agreement.c.chat_id == chat_id)
        )


async def new_agreement(dp, chat_id):
    async with dp.pool.acquire() as conn:
        return await conn.fetchrow(Agreement.insert().values(chat_id=chat_id))
