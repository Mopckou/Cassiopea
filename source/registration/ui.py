from aiogram import types
from aiogram import Dispatcher


def registration():
    keyboard_markup = types.InlineKeyboardMarkup(row_width=1)
    keyboard_markup.row(
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.BUTTON_ACCEPT_REGISTRATION,
            callback_data="registration",
        ),
    )

    return keyboard_markup


def accept_registration():
    keyboard_markup = types.InlineKeyboardMarkup(row_width=1)
    keyboard_markup.row(
        types.InlineKeyboardButton(
            Dispatcher.get_current().lang.BUTTON_ACCEPT_REGISTRATION,
            callback_data="accept registration",
        ),
    )

    return keyboard_markup
