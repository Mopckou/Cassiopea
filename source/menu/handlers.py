from bot import dp
from aiogram import types
from .func import ActionFilter, unregistration, remove_client
from .ui import show, delete, confirm_delete
from ..helpers import check_user
from ..registration.func import clients_list


__all__ = [
    "client_profile",
    "menu_of_delete",
    "delete_client"
]


@dp.callback_query_handler(ActionFilter('account_'))
async def client_profile(query: types.CallbackQuery):
    *_, client = query.data.split('_')
    menu = f'*{client}*\n\n'

    await query.message.edit_text(menu, reply_markup=delete(client))


@dp.callback_query_handler(ActionFilter('menu of delete_'))
async def menu_of_delete(query: types.CallbackQuery):
    *_, client = query.data.split('_')

    await query.message.edit_text(
        dp.lang.CONFIRM_DELETING.format(client),
        reply_markup=confirm_delete(client)
    )


@dp.callback_query_handler(ActionFilter('delete_'))
async def delete_client(query: types.CallbackQuery):
    chat_id = query.from_user.id
    *_, client = query.data.split('_')

    if not await remove_client(dp, chat_id, client):
        await query.answer(dp.lang.DELETE_FAIL, show_alert=True)
        return await query.message.edit_text(
            dp.lang.ACCOUNT_CHOICE, reply_markup=show(await clients_list(dp, chat_id))
        )

    await query.answer(dp.lang.DELETE_SUCCESS, show_alert=True)
    await check_user(dp, chat_id, query)
