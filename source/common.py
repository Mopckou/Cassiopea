

class Handlers:
    @property
    def callback_handlers(self):
        from source.agreement.handlers import confirm_agreement, proposal_of_agreement
        from source.language.handlers import confirm_language
        from source.registration.handlers import process_registration, confirm_registration

        return (
            confirm_agreement,
            proposal_of_agreement,
            confirm_language,
            process_registration,
            confirm_registration
        )

    @property
    def message_handlers(self):
        from source.language.handlers import command_language
        from source.registration.handlers import input_name, input_api_secret, input_api_key

        return command_language, input_name, input_api_secret, input_api_key



