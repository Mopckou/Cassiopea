common:
	pip install --extra-index-url https://pkgs.dev.azure.com/ermakova/Exchange/_packaging/ExchangeCommon/pypi/simple/ exchangeCommon==0.0.1

req:
	pip install -r requirements.txt

format:
	black .
