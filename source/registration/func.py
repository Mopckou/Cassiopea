from db import Client
from config import EXCHANGE_API, external_aes_decipher, request
from string import ascii_letters, digits, punctuation

available_words = ascii_letters + digits + punctuation


def good_symbols(text):
    for word in text:
        if word not in available_words:
            return False
    return True


def check_secret_key(secret_key):
    return 'S-' in secret_key


def check_api_key(api_key):
    return 'K-' in api_key


async def get_one_client(dp, chat_id):
    # TODO вернуть [0]
    return await get_clients(dp, chat_id, count=1)


async def get_clients(dp, chat_id, count=None):
    query = Client.select().where(Client.c.chat_id == chat_id)
    if count:
        query.limit(count)

    async with dp.pool.acquire() as conn:
        return await conn.fetch(query)


async def clients_list(dp, chat_id):
    return [client['name'] for client in await get_clients(dp, chat_id)]


async def new_client(dp, chat_id, name, password, token):
    async with dp.pool.acquire() as conn:
        query = Client.insert().values(chat_id=chat_id, name=name, password=password, token=token)
        await conn.execute(query)


async def register_new_client(name, chat_id, api_key, api_secret):
    url = EXCHANGE_API + "/exmo/registration"
    user_data = {
        "chat_id": chat_id,
        "user_name": name,
        "api_key": external_aes_decipher.encrypt(api_key),
        "api_secret": external_aes_decipher.encrypt(api_secret),
    }
    async with request.post(url, json=user_data) as r:
        return r.status, await r.text()


async def token_inquery(name, password):
    url = EXCHANGE_API + "/token"
    user_data = {
        "name": name,
        "password": external_aes_decipher.encrypt(password),
    }
    async with request.get(url, json=user_data) as r:
        return r.status, await r.json()


def hide_key(key):
    return key[:5] + '********'

