from aiogram.dispatcher.filters import Filter
from aiogram.dispatcher import Dispatcher
from aiogram.types import CallbackQuery
from config import request, EXCHANGE_API
from ..registration.func import get_one_client
from db import Client


class ActionFilter(Filter):

    def __init__(self, action):
        self.action = action

    async def check(self, query: CallbackQuery) -> bool:
        return query.data.startswith(self.action)


async def unregistration(dp: Dispatcher, chat_id: int) -> object:
    url = EXCHANGE_API + "/exmo/"
    headers = {"Auth": (await get_one_client(dp, chat_id))[0]['token']}
    async with request.delete(url, headers=headers) as r:
        return r.status, await r.text()


async def _remove(dp, client):
    async with dp.pool.acquire() as conn:
        await conn.execute(
            Client.delete().where(Client.c.name == client)
        )


async def remove_client(dp, chat_id, client):
    code, text = await unregistration(dp, chat_id)
    if code not in [200, 404]:
        return False

    await _remove(dp, client)
    return True
