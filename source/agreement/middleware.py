from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from common.repository import Repository, Cache
from common.aio_type import AioTypesNotFound
from .func import get_agreement
from .ui import accept_agreement
from source.common import Handlers


class AgreementMiddleware(BaseMiddleware, Handlers):
    def __init__(self,):
        self.cache = Cache()
        self.dispatcher = None
        self.handler = None
        self.chat_id = None
        super(AgreementMiddleware, self).__init__()

    async def on_process_callback_query(self, *args, **kwargs):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        query = args[0]
        if not isinstance(query, types.CallbackQuery):
            raise AioTypesNotFound

        self.chat_id = query.from_user.id

        if await self.process_agreement():
            return

        if self.handler in self.callback_handlers:
            return

        await query.message.edit_text(
            self.dispatcher.lang.PROPOSAL_OF_AGREEMENT, reply_markup=accept_agreement()
        )
        raise CancelHandler()

    async def on_process_message(self, *args, **kwargs):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        message = args[0]
        if not isinstance(message, types.Message):
            raise AioTypesNotFound

        self.chat_id = message.from_user.id

        if await self.process_agreement():
            return

        await message.answer(
            self.dispatcher.lang.PROPOSAL_OF_AGREEMENT, reply_markup=accept_agreement()
        )
        raise CancelHandler()

    async def process_agreement(self):
        if (
            self.chat_id in self.cache
            and self.cache[self.chat_id].agreement is not None
        ):
            self.cache[self.chat_id].update_time()
            self.cache.remove_expired()
            return True

        elif self.chat_id in self.cache and self.cache[self.chat_id].agreement is None:
            self.cache[self.chat_id].agreement = True
            self.cache[self.chat_id].update_time()
            self.cache.remove_expired()
            return True

        agreement = await get_agreement(self.dispatcher, self.chat_id)
        if agreement:
            self.cache[self.chat_id] = Repository(agreement=True)
            self.cache.remove_expired()
            return True


agreement_middleware = AgreementMiddleware()
