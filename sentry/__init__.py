from raven import Client
from aiogram.dispatcher import Dispatcher
from config import SENTRY_DSN, IGNORE_EXCEPTIONS


import os
import sys
import json
import logging

from raven import Client
from raven_aiohttp import QueuedAioHttpTransport


class AioHttpHumbleTransport(QueuedAioHttpTransport):
    """ Проверяет размер отправляемых хэдеров, так как в данный момент
    в aiohttp отсутствует возможность поменять максимально допустимый
    их размер (8190 байт) и Sentry иногда его может превышать.
    """
    def __init__(self, *args, **kwargs):
        workers_count = int(os.getenv('SENTRY_TRANSPORT_WORKERS_COUNT', 1))
        queue_size = int(os.getenv('SENTRY_TRANSPORT_QUEUE_SIZE', 1000))
        super().__init__(*args, workers=workers_count, qsize=queue_size, **kwargs)

    def _async_send(self, url, data, headers, success_cb, failure_cb):
        headers_size = sys.getsizeof(json.dumps(headers))
        if headers_size >= 8190:
            logging.warning(
                f'Длина хэдеров {headers_size} при отправке в Sentry превысила допустимое значение 8190!'
            )
        else:
            super()._async_send(url, data, headers, success_cb, failure_cb)


async def init_sentry(dp: Dispatcher):
    if SENTRY_DSN:
        dp.sentry = Client(SENTRY_DSN, transport=AioHttpHumbleTransport, ignore_exceptions=IGNORE_EXCEPTIONS)
    else:
        dp.sentry = None


async def close_sentry(dp):
    if dp.sentry:
        dp.sentry.remote.get_transport().close()
