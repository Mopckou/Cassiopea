import os
import sys
import json
import logging

from aiohttp import web
from raven import Client
from raven_aiohttp import QueuedAioHttpTransport


class AioHttpHumbleTransport(QueuedAioHttpTransport):
    """ Проверяет размер отправляемых хэдеров, так как в данный момент
    в aiohttp отсутствует возможность поменять максимально допустимый
    их размер (8190 байт) и Sentry иногда его может превышать.
    """
    def __init__(self, *args, **kwargs):
        workers_count = int(os.getenv('SENTRY_TRANSPORT_WORKERS_COUNT', 1))
        queue_size = int(os.getenv('SENTRY_TRANSPORT_QUEUE_SIZE', 1000))
        super().__init__(*args, workers=workers_count, qsize=queue_size, **kwargs)

    def _async_send(self, url, data, headers, success_cb, failure_cb):
        headers_size = sys.getsizeof(json.dumps(headers))
        if headers_size >= 8190:
            logging.warning(
                f'Длина хэдеров {headers_size} при отправке в Sentry превысила допустимое значение 8190!'
            )
        else:
            super()._async_send(url, data, headers, success_cb, failure_cb)


async def init_sentry(app: web.Application):
    sentry_dsn = app['config']['SENTRY_DSN']

    try:
        ignore_exceptions = app['config']['SENTRY_CONFIG']['IGNORE_EXCEPTIONS']
    except KeyError:
        ignore_exceptions = []

    if sentry_dsn:
        app['sentry'] = Client(sentry_dsn, transport=AioHttpHumbleTransport, ignore_exceptions=ignore_exceptions)
    else:
        app['sentry'] = None
