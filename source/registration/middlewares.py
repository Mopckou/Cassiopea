from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from common.repository import Repository, Cache
from common.aio_type import AioTypesNotFound
from .func import get_one_client
from .ui import registration
from source.common import Handlers
from .states import RegistrationState


class RegistrationMiddleware(BaseMiddleware, Handlers):
    def __init__(self,):
        self.cache = Cache()
        self.dispatcher = None
        self.handler = None
        self.chat_id = None
        super(RegistrationMiddleware, self).__init__()

    async def on_process_callback_query(self, *args, **kwargs):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        query = args[0]
        if not isinstance(query, types.CallbackQuery):
            raise AioTypesNotFound

        self.chat_id = query.from_user.id

        if await self.process_registration():
            return

        if self.handler in self.callback_handlers:
            return

        await query.message.edit_text(
            self.dispatcher.lang.PROPOSAL_OF_REGISTRATION, reply_markup=registration()
        )

        raise CancelHandler()

    async def on_process_message(self, *args, **kwargs):
        self.dispatcher = Dispatcher.get_current()
        self.handler = current_handler.get()

        message = args[0]
        if not isinstance(message, types.Message):
            raise AioTypesNotFound

        self.chat_id = message.from_user.id

        if await self.process_registration():
            return

        if self.handler in self.message_handlers:
            return

        await message.answer(
            self.dispatcher.lang.PROPOSAL_OF_REGISTRATION, reply_markup=registration()
        )
        raise CancelHandler()

    async def process_registration(self):
        if self.chat_id in self.cache and self.cache[self.chat_id].user_data:
            self.cache[self.chat_id].update_time()
            self.cache.remove_expired()
            return True

        user = await get_one_client(self.dispatcher, self.chat_id)
        if not user:
            return False

        if self.chat_id in self.cache:
            self.cache[self.chat_id].user_data = user
            self.cache[self.chat_id].update_time()
            self.cache.remove_expired()
            return True

        self.cache[self.chat_id] = Repository(user_data=user)
        self.cache.remove_expired()
        return True


registration_middleware = RegistrationMiddleware()
