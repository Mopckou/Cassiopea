import time


class Cache(dict):
    def __new__(cls):
        # Перекрываем создание объекта класса
        if not hasattr(cls, "instance"):
            cls.instance = super(Cache, cls).__new__(cls)
        return cls.instance

    def remove_expired(self):
        expired_users = [user for user in self if self[user].is_expired()]
        [self.pop(user) for user in expired_users]


class Repository:
    TIMER = 12  # часов

    def __init__(self, chat_id=None, language=None, user_data=None, agreement=None):
        self.time = time.time()
        self.chat_id = chat_id
        self.language = language
        self.user_data = user_data
        self.agreement = agreement

    def update_time(self):
        self.time = time.time()

    def is_expired(self):
        return (time.time() - self.time) // 3600 > self.TIMER
