from aiogram import types


class AioTypesNotFound(Exception):
    def __repr__(self):
        return "Aio type not found!"


class AioType:
    AIOGRAM_TYPES = (types.Message, types.CallbackQuery)

    def __init__(self):
        self.__type = None

    def get_first_aio_type(self, *args, **kwargs):
        for argument in list(args) + list(kwargs.values()):
            if self.__in(argument, self.AIOGRAM_TYPES):
                self.__type = argument

    def get_aio_type(self, *args, aio_type, **kwargs):
        for argument in list(args) + list(kwargs.values()):
            if self.__in(argument, [aio_type]):
                self.__type = argument

    @staticmethod
    def __in(obj, aio_types):
        return [aio_object for aio_object in aio_types if isinstance(obj, aio_object)]

    @property
    def send_text(self):
        if isinstance(self.__type, types.Message):
            return self.__type.answer
        return self.__type.message.edit_text

    @property
    def chat_id(self):
        return self.__type.from_user.id

    @property
    def value(self):
        return self.__type
