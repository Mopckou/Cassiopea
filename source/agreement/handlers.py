from aiogram import types
from bot import dp
from .func import new_agreement, get_agreement
from .ui import accept_agreement
from ..helpers import check_user

__all__ = ["proposal_of_agreement", "confirm_agreement"]


@dp.callback_query_handler(text="proposal of agreement")
async def proposal_of_agreement(query: types.CallbackQuery):
    await query.message.edit_text(
        dp.lang.PROPOSAL_OF_AGREEMENT, reply_markup=accept_agreement()
    )


@dp.callback_query_handler(text="accept agreement")
async def confirm_agreement(query: types.CallbackQuery):
    chat_id = query.from_user.id
    agreement = await get_agreement(dp, chat_id)
    if not agreement:
        await new_agreement(dp, chat_id)

    await check_user(dp, chat_id, query)
    #await query.message.edit_text(str(query.data) + " " + "LANGUAGE", reply_markup=None)
