# Models

Модели и миграция

### Запуск базы локально:
База запускается посредством docker-compose. База запускается с портом 5439.

Запустить: `docker-compose -f docker-compose/docker-compose.yaml up --build`
Linux: sudo ....

Остановить и удалить контейнер: `docker-compose -f docker-compose/docker-compose.yaml down`

### Основные понятия:

Перед тем как запускать миграцю, нужно в переменные среды добавить ссылку на базу.
Команда: `export SQLALCHEMY_DATABASE_URI=postgresql://cassa:postgres@0.0.0.0:5439/cassa_db`

Для работы с БД нужно использовать две команды migrate и upgrade.
Команда migrate генерит python скрипт и складывает в папочку migrations/versions.
Команда upgrade накатывает на базу (по ссылочке *SQLALCHEMY_DATABASE_URI*) все сгенерированные скрипты в versions.
Сгенерированные скрипты представляют собой - все изменения, которые мы вносим в модели нашу БД (models/models.py)

### Порядок действий:
1. Запустить базу (см. пункт Запуск базы локально)
2. Указать переменную среды (см. пункт Основные понятия)
    Как только база запущена, нужно проверить что к ней можно приконектиться.
    Либо в pgAdmin4, либо в терминале: psql -h localhost my_data_base postgres -p 5439
3. Теперь у нас есть чистая база, нужно накатить в нее таблицы: `FLASK_APP=migrations/migrate.py flask db upgrade`

    Вывод консоли:
    
    * `INFO  [alembic.runtime.migration] Context impl PostgresqlImpl.'
    * 'INFO  [alembic.runtime.migration] Will assume transactional DDL.'
    * 'INFO  [alembic.runtime.migration] Running upgrade  -> 0660fe67a1e5, empty message`
    
    **Не забываем добавлять в GIT сгенерированные файлы!**
    
4. Если захотели сделать изменения в базе, нужно сгенерить скрипты.
    После всех изменений выполняем команду: `FLASK_APP=migrations/migrate.py flask db migrate -m 'create_table_user'`
    Где после -m идет короткое имя новой миграции, чтобы легко находить по названию
    
    Вывод консоли:
    - `INFO  [alembic.runtime.migration] Context impl PostgresqlImpl.'
    - 'INFO  [alembic.runtime.migration] Will assume transactional DDL.'
    - 'INFO  [alembic.autogenerate.compare] Detected added table 'user''
    - ' Generating /Users/aleksejermakov/Documents/models/migrations/versions/0683f61d8f75_create_table_user.py ...  done'
`

5. После того как создали миграцию, нужно накатить на базу новые изменения, поэтому нужно повторить пункт 3.