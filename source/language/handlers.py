from bot import dp
from aiogram import types
from .ui import choose_language

from .middleware import language_middleware
from .func import get_user_language, input_language, update_language
from ..helpers import check_user


__all__ = ["command_language", "confirm_language"]


@dp.message_handler(state="*", commands=["language"])
async def command_language(message: types.Message):
    await message.answer(dp.lang.ENTER_LANGUAGE, reply_markup=choose_language())


@dp.callback_query_handler(state="*", text="rus")
@dp.callback_query_handler(state="*", text="eng")
async def confirm_language(query: types.CallbackQuery):
    chat_id = query.from_user.id
    if await get_user_language(dp, chat_id):
        await update_language(dp, chat_id, query.data)
    else:
        await input_language(dp, chat_id, query.data)
    language_middleware.clear_cache(chat_id)
    await language_middleware.process_language()

    await check_user(dp, chat_id, query)
