from aiogram.dispatcher import Dispatcher
from db import Client


def request_(exchange_api, external_decipher):
    def wrapper(func):
        async def _wrapper(*args, **kwargs):
            dp = Dispatcher.get_current()

            url = args[1]
            print("what a url", url)
            if exchange_api not in url:
                return await func(*args, **kwargs)

            result = await func(*args, **kwargs)
            print("новый разпрос")
            if result.status == 401:
                if "headers" not in kwargs or "Auth" not in kwargs["headers"]:
                    return result

                user = await get_auth_user_info(dp, kwargs["headers"]["Auth"])
                if not user:
                    return result
                print("запрашиваем новый токен!")
                res = await func(
                    method="GET",
                    str_or_url=(exchange_api + "/token"),
                    headers={"Content-Type": "application/json"},
                    json={
                        "name": user["name"],
                        "password": external_decipher.encrypt(user["password"]),
                    },
                )
                if res.status != 200:
                    return result

                new_token = (await res.json())["token"]
                print("новый токен", new_token)
                await update_token(dp, user["chat_id"], new_token)
                kwargs["headers"]["Auth"] = new_token
                result = await func(*args, **kwargs)

            return result

        return _wrapper

    return wrapper


async def get_auth_user_info(dp, token):

    async with dp.pool.acquire() as conn:
        return await conn.fetchrow(Client.select().where(Client.c.token == token))


async def update_token(dp, chat_id, token):
    async with dp.pool.acquire() as conn:
        return await conn.fetchrow(
            Client.update().where(Client.c.chat_id == chat_id).values(token=token)
        )
