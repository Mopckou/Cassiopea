from aiogram.dispatcher.filters.state import State, StatesGroup


class RegistrationState(StatesGroup):

    NORMAL_STATE = State()
    INPUT_NAME = State()
    INPUT_API_KEY = State()
    INPUT_API_SECRET = State()
    ACCEPT_REGISTRATION = State()
