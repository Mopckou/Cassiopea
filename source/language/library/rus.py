YES = 'Да'
NO = 'Нет'
ENTER_NAME = "Введите имя аккаунта!"
ENTER_API_KEY = "Введите API KEY"
SUCCESS_API_KEY = "Вы ввели api key!"
ENTER_API_SECRET = "Введите API SECRET"
SUCCESS_API_SECRET = "Вы ввели api secret!"
ENTER_LANGUAGE = "Выберите язык!"
BUTTON_ACCEPT_AGREEMENT = "Согласен"
ACCEPT_AGREEMENT = "Согласен"
BUTTON_PROPOSAL_OF_AGREEMENT = "Регистрация"
PROPOSAL_OF_AGREEMENT = (
    "Нажимая кнопку Вы осознаете весь риск торговли на криптовалютной бирже.\n"
    "И не имеете никаких претензий к разработчикам бота."
)
PROPOSAL_OF_REGISTRATION = (
    "Данный бот предоставляет инструмент автоматизированной торговли на криптовалютной бирже EXMO.\n\n"
    "Чтобы начать нужно пройти короткую процедуру регистрации!"
)
BUTTON_ACCEPT_REGISTRATION = "Зарегистрироваться"
REGISTRATION_INFO = 'Имя: {}\nAPI KEY: {}\nAPI SECRET: {}\n\nПодтвердите регистрацию'
ACCOUNT_CHOICE = "Выберите аккаунт:\n"
KEY_INSTRUCTION = "Инструкция по генерации API ключей!"
BUTTON_REGISTER_NEW_ACCOUNT = "Зарегистрировать новый"
REGISTRATION_SUCCESS = 'Регистрация успешно проведена!'
REGISTRATION_FAILED = "Зарегистрироваться не удалось, попробуйте позже!"
MESSAGE_ERROR = "Упс. произошла ошибка! :C\nРазработчики уже уведомлены!"
KEYS_EXISTS = "Пользователь с такими ключами уже зарегистрирован!"
BAD_SYMBOLS = "Сообщение должно состоять из цифр и английских букв!"
BAD_API_SECRET = 'Ключ должен начинаться с "S-"'
BAD_API_KEY = 'Ключ должен начинаться с "K-"'
BAD_KEYS = 'Подключиться с предоставленными API ключами не удалось!'
DELETE_CLIENT = 'Удалить аккаунт'
CONFIRM_DELETING = 'Вы точно хотите удалить аккаунт: {}?'
DELETE_SUCCESS = 'Аккаунт успешно удален!'
DELETE_FAIL = 'Ошибка удаления аккаунта, попробуйте попозже!'
