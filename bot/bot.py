import logging
from config import TOKEN, SOCKS
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from source.user.middleware import user_middleware
from source.language.middleware import language_middleware
from source.agreement.middleware import agreement_middleware
from source.registration.middlewares import registration_middleware


logging.basicConfig(level=logging.INFO)


def safer(func):
    """
    Декоратор для отлавливания ошибок в коде.
    Использовать только с message handlers.

    Пример использования:

    @dp.message_handler(regexp=' ... ')
    @safer
    async def echo(message: types.Message):
        ...

    """

    async def decorator(message: types.Message):
        try:
            result = await func(message)
        except Exception as ex:
            logging.exception(f"Ошибка: {ex}")
            return await message.reply_sticker(
                sticker="CAADAgAD1QYAAoVqUgipJB1dL2dFyhYE"
            )
        return result

    return decorator


bot = Bot(token=TOKEN, proxy=SOCKS)
storage = MemoryStorage()


dp = Dispatcher(bot, storage=storage)
dp.middleware.setup(LoggingMiddleware())
dp.middleware.setup(user_middleware)
dp.middleware.setup(language_middleware)
dp.middleware.setup(agreement_middleware)
dp.middleware.setup(registration_middleware)
