from db import Chat


async def get_user(dp, chat_id):
    async with dp.pool.acquire() as conn:
        return await conn.fetchrow(
            Chat.select().where(Chat.c.id == chat_id)
        )


async def new_user(dp, chat_id):
    async with dp.pool.acquire() as conn:
        query = Chat.insert().values(id=chat_id)
        await conn.execute(query)
