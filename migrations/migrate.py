# -*- coding: utf-8 -*-
# старт flask-приложения
import os

from flask import Flask
from flask_migrate import Migrate

from db.common import db


SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI")
if not SQLALCHEMY_DATABASE_URI:
    raise SystemExit(u"Отсутствует обязательный параметр SQLALCHEMY_DATABASE_URI")

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
db.init_app(app)
migrate = Migrate(app, db)
