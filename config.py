from os import environ as env
import aiohttp
from common.aes import AESCipher
from source.decorators import request_

TOKEN = "744261489:AAHdbjrfQG92IDxZX6K34ciMsCOjadvsA-o"
SOCKS = "socks5://127.0.0.1:9050"
SQLALCHEMY_DATABASE_URI = env["SQLALCHEMY_DATABASE_URI"]
EXCHANGE_API = env.get("EXCHANGE_API", "http://0.0.0.0:5001")
data = SQLALCHEMY_DATABASE_URI.split("@")
POSTGRES_USER = data[0].split(":")[1].split("//")[1]
POSTGRES_PASSWORD = data[0].split(":")[2]
POSTGRES_HOST = data[1].split(":")[0]
POSTGRES_PORT, POSTGRES_DATABASE = data[1].split(":")[1].split("/")
INTERNAL_KEY = env["INTERNAL_KEY"]
EXTERNAL_KEY = env["EXTERNAL_KEY"]
internal_aes_decipher = AESCipher(INTERNAL_KEY)
external_aes_decipher = AESCipher(EXTERNAL_KEY)
request = aiohttp.ClientSession()
request._request = request_(EXCHANGE_API, external_aes_decipher)(request._request)
SENTRY_DSN = env["SENTRY_DSN"]
IGNORE_EXCEPTIONS = []
